<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 11:59 AM
 */

namespace App\BookTitle;
use App\Model\Database as DB;



class BookTitle extends DB
{

    private $id;

    private $book_name;

    private $author_name;

    public function __construct()
    {

        if(!isset($_SESSION))
            session_start();


    }

    public function setData($posData){

        if(array_key_exists('id',$posData)){

            $this->id=$posData['id'];

        }
        if(array_key_exists('bookname',$posData)){

            $this->book_name=$posData['bookname'];

        }
        if(array_key_exists('authorname',$posData)){

            $this->author_name=$posData['authorname'];

        }
    }

    public function store(){


        $arrData=array($this->book_name,$this->author_name);

        $sql="INSERT into book_title(book_name,author_name)VALUES (?,?)"; //? ?(Placeholder) have been used for avoiding sql injection

        $STH=$this->DBH->prepare($sql);

        $result=$STH->execute($arrData);

        Utility::redirect("create.php");


    }

    public function index(){

        echo "I am inside:".__METHOD__."<br>";

    }


}